import { useRouter } from 'next/router'

export default ({ href, children }) => {
  if (href[0] === '@') {
    const path = useRouter().pathname
    href = path + (path !== '/' ? '/' : '') + href.slice(1)
  }
  
  return (
    <a href={href}>{children}</a>
  )
}