export default (name = '') => {
  const extSteps = name.split('.')
  return (
    extSteps.length > 1 
    ? extSteps[ extSteps.length - 1 ] 
    : ''
  )
}