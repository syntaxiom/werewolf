const withPlugins = require('next-compose-plugins')
const withMDX = require('@next/mdx')
const images = require('remark-images')
const emoji = require('remark-emoji')
const withCSS = require('@zeit/next-css')
const path = require('path')

module.exports = withPlugins(
  [
    withMDX({
      extension: /\.mdx?$/,
      options: {
        remarkPlugins: [ images, emoji ]
      }
    })
  ],
  withCSS({
    cssModules: true,
    pageExtensions: [ 'js', 'jsx', 'md', 'mdx' ],
    webpack: config => {
      config.resolve.alias['~'] = path.resolve(__dirname);
      return config;
    }
  })
)