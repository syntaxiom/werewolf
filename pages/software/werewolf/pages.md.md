# /pages

Where all the website pages live ~~in case that wasn't obvious enough~~.

## It just works.:tm:

Every page is written in either

1. Markdown (.md); or
2. Markdown Extended (.mdx).

[MDX.js](https://mdxjs.com/) is a JSX-in-Markdown library, which means React components can be placed in an otherwise dull page. Instead of writing "index.html", there's "index.mdx" with labeled `<Directory>` components (e.g. "First things first." followed by "README.md", "AUTHORS.txt", etc.) Each "file" is actually a Markdown file, so *"hello.txt"* in reality is *"hello.txt.md"*.

## All together now.

Rendering occurs in **_app.js**, where all the app-wide styles, static assets, and metadata are loaded. It also renders the site icon and dynamically changes the page's title. Instead of stacking multiple React components together, everything goes through this file first, thus permitting the use of themes, layouts, and repeated logic.