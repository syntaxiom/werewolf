# /shared

Where all the dynamic assets and utilities reside.

## DRY (Don't Repeat Yourself).

Components and pages sometimes require the same functionality for a specific part. Instead of writing the same part 100 times, these functions can be called from 1 place 100 times. Let's look at 2 of these.

## colors.js

exports a POJO (Plain Old JavaScript Object) containing `{file extension, color}` pairs. This permits the use of themes to create a consistent user experience. *Simple as that.*

## extension.js

exports a function which extracts the file extension from a given string. If no extension exists, it returns a null string. *Simple as **that**.*