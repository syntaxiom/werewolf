# /components

Where all of the React components live.

## It simply functions.:tm:

**Functional components** are the way to go for this ~~and basically every other~~ React project. Every component resides in its own folder as "index.jsx" (e.g. "Directory" -> "/Directory/index.jsx") so that any styles, tests, etc are locally scoped; renaming the component requires only renaming the folder. We'll briefly discuss 2 components used in this project.

## "Directory"

provides 2 props: **label**, and **files**. For example, "Main folders:" is this directory's *label*, and "pages.md", "components.md", etc are its *files*. Files without extensions become folders, and the label can be left blank if desired.

## "HotLink"

provides 1 prop: **href**. This wraps a normal `<a>` element with the ability to replace "@" with the full URL path, thus permitting relative links. For example, "@components.md" (inside "/about") becomes "/about/components.md".