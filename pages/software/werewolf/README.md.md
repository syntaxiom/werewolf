# TL;DR

Everything is rolled into Next.js:

1. "File" pages are written in Markdown (MD and MDX);
2. "Directory" components are written in JSX;
3. Other utilities and assets are placed in appropriate folders; and
4. The website is pushed to GitLab where it's deployed to Zeit hosting (Now.sh).

Now I can write content, add features, and fix bugs from the comfort of my code editor, just how ~~Linux pedants~~ I like it.