# /next.config.js

This is my only real complaint about Next.js: configuring plugins sucks.
It's an ugly file and I wish Zeit provided a more idiomatic approach, rather than defer all responsibility to the community.
In any case, the list of plugins this project uses are as follows:

1. [next-compose-plugins](https://github.com/cyrilwanner/next-compose-plugins) = multiple plugins, one config object;
2. [@next/mdx](https://github.com/zeit/next.js/tree/master/packages/next-mdx) = Markdown Extended (MDX) rendering;
3. [remark-images](https://github.com/remarkjs/remark-images) = improved image syntax for Markdown;
4. [remark-emoji](https://github.com/rhysd/remark-emoji) = ":emoji:" syntax for Markdown; and
5. [@zeit/next-css](https://github.com/zeit/next-plugins/tree/master/packages/next-css) = import CSS (and CSS modules) anywhere in the project.

One day, there will be a universal plugin system for these frameworks (Next.js, Gatsby.js, Nuxt, Scully, etc.) One day...