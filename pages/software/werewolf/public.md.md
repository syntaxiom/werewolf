# /public

Where all the app-wide assets live.

## Rhyme and reason.

Next.js permits the use of "/public" (where other frameworks might choose "/static") for static assets. These include

1. _app.css (not a built-in like "_app.js")
2. file.css (as well as "file-md.css", "file-txt.css", etc)
3. favicon.png

as well as "manifest.json", "robots.txt", etc.