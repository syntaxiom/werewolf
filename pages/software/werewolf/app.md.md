# /pages/_app.js

A provision by Next.js to pipe all pages through one universal component.
This is helpful for themes, layouts, necessary API calls, etc.
In addition to themes and layouts, this project uses "_app.js" for the following reasons:

1. Add text decoration to Markdown "files";
2. Replace each `<a>` element with **[HotLink](@../components.md)**;
3. Set the page title (e.g. "werewolf - app.md");
4. Include all the necessary static assets;
5. Modify the nav bar; and
6. Apply the appropriate styles to the current route.

Markdown is rendered by `<MDXProvider>` and then passed to the page's component. Boom, instant React pages!