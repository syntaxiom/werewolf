# The butt-end of Unity projects

Originally a work-related project, this was my first time leveraging GCP for a non-web app.
While the Unity Firebase SDK wasn't *bad*, it wasn't *good* either ~~like the current DOTS implementation~~.
I ended up leveraging Cloud Functions to create an API which hooks all our back-end needs.
Unfortunately, I never got to see this project all the way through, but I would like to finish it someday.

This project contains a Unity demo, an admin web app demo, and the back-end code in question.