# *Easy* authentication!

2 functions were created to interface with Firebase Auth.

## [dummyLogin](https://gitlab.com/syntaxiom/Phoenix/blob/master/functions/src/index.ts#L30)

Log users in via username and password.
The Firebase Admin SDK provides a built-in method for logging in users via *email* and password, so I created a workaround:

- Get the username and password;
- Fetch the email from the User document with said username; and
- Use the Auth API as normal.

## [dummySignup](https://gitlab.com/syntaxiom/Phoenix/blob/master/functions/src/index.ts#L47)

Sign up users via email, username, and password.
This was never fleshed out; it just responds with a success or error message.