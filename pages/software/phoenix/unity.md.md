# If you login the game you login for real!

So long as the user authentication mechanism was an API, I used standard .NET HTTP methods.
This functionality was *mostly* decoupled from Unity: the "Auth" class inherits "MonoBehaviour" to make the game-implementation easier.
Unfortunately, the only feedback is "Debug.Log()" due to time constraints.

## [Auth](https://gitlab.com/syntaxiom/Phoenix/blob/master/UnityFirebaseExample/Assets/Scripts/Auth.cs)

"Auth" centralized all user authentication with the following methods:

- [Response](https://gitlab.com/syntaxiom/Phoenix/blob/master/UnityFirebaseExample/Assets/Scripts/Auth.cs#L13) = issue the request and provide the response
- [Login](https://gitlab.com/syntaxiom/Phoenix/blob/master/UnityFirebaseExample/Assets/Scripts/Auth.cs#L22) = call "Response" with the user info and the "dummyLogin" endpoint
- [Signup](https://gitlab.com/syntaxiom/Phoenix/blob/master/UnityFirebaseExample/Assets/Scripts/Auth.cs#L44) = call "Response" with the user info and the "dummySignup" endpoint

## [LoginButton](https://gitlab.com/syntaxiom/Phoenix/blob/master/UnityFirebaseExample/Assets/Scripts/LoginButton.cs) & [SignupButton](https://gitlab.com/syntaxiom/Phoenix/blob/master/UnityFirebaseExample/Assets/Scripts/SignupButton.cs)

These buttons simply call their respective "Auth" methods.