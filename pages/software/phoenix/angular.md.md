# Real-time user creation!

This web app was meant to demonstrate the interoperability between

- Firebase (console),
- Unity, and
- Web

as a faux admin app.
It's a default-generated Angular 8 app, intended only to demonstrate basic functionality (hence the garish styles.)
You can create a new user from the Firebase console, or from the Unity demo, and that user will show up in real-time!

## [app.component.html](https://gitlab.com/syntaxiom/Phoenix/blob/master/web/src/app/app.component.html#L331)

- Iterate over each user, and pipe through "async" since data is fetched asynchronously.
- Display the user's email and their alias/username.
- Wrap the list in a red/pink box.

## [app.component.ts](https://gitlab.com/syntaxiom/Phoenix/blob/master/web/src/app/app.component.ts)

- Set "users" to be an Observable of type Array
- Fetch the users from Firestore
- Update "users" if value changes are detected