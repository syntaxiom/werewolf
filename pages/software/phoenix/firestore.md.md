# *Easy* NoSQL database!

1 function was added to interface with Cloud Firestore.

## [makeUserDoc](https://gitlab.com/syntaxiom/Phoenix/blob/master/functions/src/index.ts#L60)

Add a new "User" document whenever a new user is created.
This would trigger automatically, and was intended to replace the "dummySignup" API if we had time.