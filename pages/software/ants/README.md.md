# Trial by fire ~~ants~~!

To land an internship at a game studio, I was tested on a few things:

- Technical skills in Unity (and C# and .NET in general)
- Communicating my thought process
- Preparing my code for potential future developers to work on

This test was administered in the form of a simple demo: make a group of ants move realistically across a table.
Every commit came with a README update, which has been separated into individual files in this folder (under "Timeline".)
I'm still quite proud of the results, even after all this time.

[Here's a demo video](https://1drv.ms/v/s!Ao4XvLYhvbsWmTFBU42QcY0yEtsR?e=XZGoaV) to give y'all an idea.