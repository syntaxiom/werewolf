# ["Ant" mechanics](https://gitlab.com/syntaxiom/Ants/tree/6c71811d604fd5213e9556d422ee4708a3ca671c)

Today's theme is "readjust." Instead of just focusing on code-side fixes, I also took time to adjust prefabs. Rather than keep all the original accessors and mutators in each class, I simplified the method calls and reduced overall complexity. Cleaning today will prevent scrubbing tomorrow.

## (Progress)

- `Ant` movement is now dictated by `Scent`
- `Scent` decay utilizes `transform.scale` for simplicity
- More components added to `Scent` prefab
  - Mesh with transparent material (for debugging)
  - Rigidbody to make trigger collisions work
- `Ant` prefab no longer has 90-degree `y` rotation
- Condensed accessors and mutators of `Ant` and `Scent` to their necessary parts
  - `Init()` method takes the place of a constructor
  - Methods are single-purpose
  - Lifecycle methods are used more efficiently
- `Lottery` now has "neutral" outcome (nothing won or lost)
- Smaller tweaks to the scene

## (TODO)

- ~~Flesh out behavior between `Ant` and `Scent`~~
- Create `AntSpawn`, maybe `GameManager`
- Scaffold a wiki