import HotLink from '~/components/HotLink'
import { useRouter } from 'next/router'
import extension from '~/shared/extension'
import Head from 'next/head'
import colors from '~/shared/colors'
import { MDXProvider } from '@mdx-js/react'

const fileMods = {
  "": {},
  "txt": {},
  "md": {
    h1: props => <h1 {...props}># {props.children}</h1>,
    h2: props => <h2 {...props}>## {props.children}</h2>,
    em: props => <em {...props}>*{props.children}*</em>,
    strong: props => <strong {...props}>**{props.children}**</strong>,
    del: props => <del {...props}>~~{props.children}~~</del>,
    li: props => <div {...props} className="tweak">— {props.children}</div>,
  },
}

for (const ext in fileMods) {
  fileMods[ext]['a'] = props => <HotLink {...props}/>
}

export default ({ Component, pageProps }) => {
  const path = useRouter().pathname
  const destSteps = path.split('/')
  
  const dest = destSteps[1] ? destSteps[ destSteps.length - 1 ] : ''
  const prev = destSteps.length > 2 ? destSteps.slice(0, -1).join('/') : '/'
  const ext = extension(path)
  
  return (
    <>
      <Head>
        <title>werewolf - {dest ? dest : '[home]'}</title>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.png"/>
        <link rel="stylesheet" href="/_app.css"/>
        
        {ext &&
        <>
          <link rel="stylesheet" href="/file.css"/>
          <link rel="stylesheet" href={`/file-${ext}.css`}/>
        </>
        }
      </Head>
      
      <div className="frame">
        <div className="frame-item">
          <div className="nav">
            <a className="home-link" href="/">[home] /</a>
            
            {dest &&
            <span>
              <a className="prev-link" href={prev}>&nbsp;(prev) /&nbsp;</a>
              <strong>{dest}</strong>
            </span>
            }
          </div>
          
          <MDXProvider components={fileMods[ext]}>
            <div className={ext ? `file ${ext}` : ""} style={ext ? {color: colors[ext]} : {}}>
              <Component {...pageProps} />
            </div>
          </MDXProvider>
        </div>
      </div>
    </>
  )
}
