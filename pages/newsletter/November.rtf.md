# Newsletter #2 - 7 November 2019

## I'm back from the depths!

Hello! It's Sean.

I journeyed through the depths of my mind and soul this month. My discoveries are fruitful to my growth, which lends perspective to those listening. Many lessons will be unpacked in this letter. Some lessons may appear obvious, depending on the present chapter of your life, but it never hurts to revisit them:

1. Writing makes ideas real. The mere act of containerizing my thoughts has been the harshest reliever of stress. I'm still growing accustomed to the breathing-room in my head.
2. Organizing makes tasks doable. It's said that paradise is knowing, not completing. Pushing things out of the way, ironically, builds stress whereas bringing them in front of me dissipates stress.
3. Planning makes productivity flourish. Instead of haphazardly coding or designing, I've become aware of how important strategy plays into getting things done. Blueprints are made before products, after all.

I internalized these lessons after facing my shortcomings and battling my demons. "Growing up" doesn't come with shortcuts, nor manuals. Rather, maturity is born from trials of self-sacrifice, through tears and doubt. My behaviors were reluctant to change. Now that I'm ready, however, I will share these matters with you.

<hr/>

### Writing makes ideas real.

Juggling all my thoughts exhausts my brain. So, I started keeping notes of tasks and ideas and priorities. The monumental relief I experienced cannot be stated with words alone. Anytime an idea, task, or priority arrived, I route it through paper or keyboard. I kept a "To-Do" list, a calendar, a journal, etc. with me at all times (albeit through Google's ecosystem.) However, that wasn't enough.

<hr/>

### Organizing makes tasks doable.

Nowadays, I maintain a personal Trello board. It has 4 columns: "Deck", "Do", "Doing", and "Done."

#### "Deck"

Every idea and task starts here as a card. Each card is then assigned a label, like "Work" or "School" or "Fun". If required, I eventually give the card a due date, after which it moves to the next column.

#### "Do"

Everything that must eventually be done arrives here. These can be upcoming school assignments, business-related tasks, etc. Because every item has a due date, I'm more likely to prioritize this column before moving a card to the next.

#### "Doing"

Everything that I'm currently doing resides here. If I need to pause something, then I'll move that item back to the top of "Deck" or "Do" depending on its priority level. Otherwise, once an item is complete, I mark it as such and place it in its final destination.

#### "Done"

Every completed task, mission, roadblock, etc eventually arrives here. Occasionally, items need to be resorted to previous columns, in case something isn't as complete as I thought. In any case, when November is over, I will archive the list and start a new "Done" list, so I can visualize my progress month-by-month.

<hr/>

### Planning makes productivity flourish.

This is the big one. My greatest fear is becoming my best friend. Mind you, "planning" and I are still learning to get along, and compromises have been made along the way, but nonetheless the relationship is sound.

It goes like this: how can I get things done when I don't know what needs to be done? There's a simple answer: I can't. So many projects of mine started but eventually dissolved because of poor planning. More precisely, I falsely believed that time spent planning was time not spent on completing said projects. If only literally everyone I've ever met someone told me that planning was a necessary prerequisite.

Once an idea is written down, and then arrives as a priority, work begins with writing, drawing, and blueprinting. Some tasks require more planning than others, but the necessity persists. Once I believe I have enough written out, the next step is prototyping. It's only here when I discover which matters I hadn't yet considered, and so I go back to planning. The alternative is becoming so overwhelmed that nothing gets accomplished at all.

Like I said, the relationship I have with "planning" is slowly developing. Presently, we're only at an acquaintanceship. To be fair, I've ignored "planning" for 24 years, so it's only natural that we'd get off to a rocky start. In due time, hopefully, this too will become an internalized habit.

<hr/>

## Back to the depths, I go!

Notice the lack of business talk? It's because my entrepreneurship couldn't proceed, not in these conditions at least. I wasn't prepared for the journey, and I not-so-secretly believe nobody else is either.

Once everything is said and done, I've learned to become more self-aware and self-responsible. No amount of TED Talks, books, interviews, or mentorship can make me grow. In order to grow, I must take the plunge when I'm ready.

And I'm ready for another dive. This time, my team is coming with me.

Best of luck wherever life takes you,

~ Sean Lester-Wilson
