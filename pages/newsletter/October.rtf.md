# Newsletter #1 - 7 October 2019

## Hello, world!

My name is Sean Lester-Wilson. I'm a young entrepreneur that you likely met at one of the business meet-ups held by CSU Monterey Bay (or you reached out to me personally, or you're a team-/family-member.) Thank you all for your valuable insight; your words helped more than you'll ever know.

I am writing this newsletter for the following reasons:

1. New perspective gives me direction in my business and life in general, so I owe you regular updates on my goings on;
2. Committing to a schedule keeps me focused and clear on where I've been, where I am, and where I'm going with regards to projects and self-growth; and
3. Sharing my experiences enables me to write my own story, rather than having my story be written for me.

Expect these letters at least once a month. That said, I might be inclined to write a quick letter about anything new and super exciting, but expect those less frequently. If, at any point, you decide to not receive these letters anymore, please let me know and I'll update the mailing list.

With all of that out of the way, let's start with recapping the previous month.

### Starting up

In late August of this year, I became clear on my path toward entrepreneurship: I would start a company that solves existing problems in computer science education. The weekend before school started, I onboarded a friend of mine as my executive assistant. Every week thereafter (until last week or so), I onboarded more team members: a business consultant, an engineer, a data analyst, and a software designer. Total, there are 6 people on the team (including myself). We work great together, but the first month and a half was a rocky start.

**First, I set expectations for product launch way, *way* too high.** I wanted to go from zero to corporation in 12-18 months. We went full steam ahead with no consideration for anyone's schedule, and I underestimated the depth of creating a fleshed out business model. Some were excited to go forward with this, some were (justifiably) wary. Of course, the team couldn't meet any of the arbitrary deadlines we set, and team members were growing fatigued. So, we reevaluated our expectations and set forth a more reasonable timeline for launch.

**Next, I wasn't as clear as I thought I was on where to take this business.** Do I make this company my life's blood, or do I build it up and sell it? Do I keep it running in perpetuity like a well-oiled machine, or is this just an experiment? The lack of clarity gave me a lot of self-doubt, and required significant self-reflection to remedy. In the end, I decided I'd like this company to eventually be self-governing so that I could learn and move on to other creative business pursuits (more on that later.)

**Finally, I wasn't making time for myself.** I have a TA job at CSUMB, and I have other creative projects on my mind, but I found myself unable to prioritize as well as I should. In short, I didn't pursue good work-life balance, and my grades (and sanity) suffered slightly because of it. This forced me to reflect on what I was doing and why I was doing it. After talking to as many people as I could, I've made "balance" my highest priority.

### Learning to reflect

Here are the things I learned during that rocky period (in no particular order):

1. Perspective is the most powerful virtue a person can have. Humans don't learn new things in a vacuum; rather, we learn new things by referencing old things (you learn arithmetic before you learn algebra, for example). Learning about ourselves is no different. When someone shares a story with me, I can compare and contrast: "Does that make sense to me? Have I been doing something similar? How well did that venture go? Is there a greater lesson to be learned?" etc. As far as I'm concerned, internalizing someone else's story is the only mirror into your own story.
2. I set my own boundaries. Of course, I can't overcome natural limitations; no amount of physical training will make me the next Kobe Bryant, no amount of education will make me the next Stephen Hawking, etc. However, I don't know what those limitations are until I pursue them. I'm doing my best to adopt the attitude of "I make the rules", rather than "I must follow the rules" when it comes to personal endeavors; I already feel more creative than I've been in a while.
3. Growth opportunities should be seized whenever possible. I'm talking about all kinds of growth: social, business, personal, monetary, etc. Going to events, partnering with others, talking with experts, learning from mentors, writing down ideas, etc are necessary for growth. A time of struggling is, by necessity, a time to grow. Growth leads to reflection. Reflection leads to perspective. Perspective leads to positive impacts in everyone's life, even those you don't know. Ergo, when you grow, you help others and you become happier.

To be clear: learning from my mistakes is hard. I have to stare at everything that makes me vulnerable, and then consciously work to improve. That said, I don't beat myself up because reflection is part of growing up. I'm fortunate to have the wherewithal to reflect on the past and think about the future. Learning is an experience unto itself.

### Other news

Before I wrap up, here's the basic rundown of my current goings on:

- The team is working diligently toward the first milestone of the MVP
- I've started a software engineering private instruction business (open to programmers of all skill levels)
- I'm taking on other business opportunities (led by some readers of this very letter)
- I'm working on other creative projects which will be published on my new website (to be revealed at a later date)

<hr/>

Stay tuned for more mistakes, more lessons, and more news!