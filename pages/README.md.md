# werewolf.dev

Or, in modern hieroglyph: :full_moon: :wolf: :computer:

This file teaches you how to navigate the site, the reason behind its existence, and what to expect during your visit.

## Navigation

Treat werewolf.dev like a toy file explorer, with the following caveats:

1. The **[home]** button takes you home ~~...Country Roads...~~; and
2. The **(prev)** button takes you to the *previous folder* (e.g. you're in "/about/README.md", so *(prev)* takes you to "/about").

## Why "werewolf.dev"?

[Why not?](@../about/README.md)

## Expectations

This site is a never-ending project, so this list must remain short:

1. Each page is optimized for desktop and mobile (landscape and portrait);
2. This site is semi-playful, semi-professional, fully self-aggrandizing; and
3. To paraphrase the author of core.js: "I'm looking for a good job."