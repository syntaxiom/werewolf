# Let's get personal.

I fell into a vicious cycle:

1. I was afraid of professional work because I didn't have a public portfolio.
2. I never published my portfolio because it wasn't impressive enough.
3. My portfolio wasn't impressive enough because I have impostor syndrome.
4. I have impostor syndrome because I hadn't done enough professional work. (Go to step 1)

I wasn't in control of my own self-worth.

Control is seized, not bestowed. The cavalry isn't coming. *I* must lead my own transformation.

Like a werewolf taking back control of his own monstrous nature.

## Who is this website for?

1. Myself, first and foremost;
2. Prospecting employers;
3. Prospecting collaborators; and
4. Myself, when it's all said and done.

## How does one get in touch?

howl@werewolf.dev

For all queries, business or otherwise.