# :purple_heart:

This site wouldn't be possible without:

## [Next.js](https://nextjs.org/);

The React framework of my dreams, created by none other than [Zeit](https://github.com/zeit); my only complaint is their plugin system.

## [MDX.js](https://mdxjs.com/);

The best library to make writable React components; JSX + Markdown = productivity.

## [Now.sh](https://zeit.co/home);

The hosting platform of my dreams, also created by Zeit; investing in their ecosystem made this project so much easier.

## [Remix Icon](https://remixicon.com/);

Their iconography is elegant, and they graciously provide their icons in .png, .png, and `<svg>`-tag formats; this is *the* open source icon library. ~~Font Awesome who?~~

## [Lorc (Thomas Tamblyn)](http://lorcblog.blogspot.com/) from [Game-icons.net](https://game-icons.net/) for [the favicon](https://game-icons.net/1x1/lorc/wolf-howl.html);

This illustration reflects the site poignantly, and that whole website has amazing CC3.0-licensed art; send them some love!

## My friends and family.

For tolerating my BS while I work on this project.